package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post) {
        //Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);

    }

    //    Get All Posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    //    Delete post
    public ResponseEntity deletePost(String stringToken,Long id){
       /* postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);*/
    Post postForDeleting = postRepository.findById(id).get();

    String postAuthorName = postForDeleting.getUser().getUsername();
    String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

    if(authenticatedUserName.equals(postAuthorName)){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post Deleted successfully",HttpStatus.OK);

    }
    else{
        return new ResponseEntity<>("You are not authorize to delete this post",HttpStatus.UNAUTHORIZED);

        }
    }

    //    Update post
    public ResponseEntity updatePost(Long id,String stringToken, Post post){
/*//        Find the post to update
        Post postForUpdate = postRepository.findById(id).get();

//        Updating the title and content
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

//        Saving and updating a post
        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);*/

        Post postForUpdating = postRepository.findById(id).get();
//  Fet the author of the specific post
        String postAuthor = postForUpdating.getUser().getUsername();
//  Get the username from the stringToken to compare it with the username of the current post being edited
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);
//  check if the username of the authenticated user matches the post's user
        if(authenticatedUserName.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully",HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorize to edit this post",HttpStatus.UNAUTHORIZED);
        }
    }

    //  Get users post
    public  Iterable<Post> getMyPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getPosts();
    }

}
